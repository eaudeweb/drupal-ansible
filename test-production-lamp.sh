#!/usr/bin/env bash

ansible-playbook -i hosts.test --vault-password-file=test-production-lamp-vault.key test-production-lamp.yml